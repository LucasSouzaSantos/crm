
<!-- start section -->
<section class="big-section bg-dark-slate-blue wow animate__fadeIn" id="contato">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center margin-six-bottom">
                <h6 class="alt-font text-white font-weight-500">Contact form style 04</h6>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-xl-5 col-lg-10 col-md-10">
                <form action="email-templates/contact-form.php" method="post">
                    <input class="input-border-bottom border-color-dark-white-transparent bg-transparent extra-large-input px-0 margin-25px-bottom border-radius-0px required" type="text" name="name" placeholder="Your name" />
                    <input class="input-border-bottom border-color-dark-white-transparent bg-transparent extra-large-input px-0 margin-25px-bottom border-radius-0px required" type="email" name="email" placeholder="Your email address" />
                    <input class="input-border-bottom border-color-dark-white-transparent bg-transparent extra-large-input px-0 margin-25px-bottom border-radius-0px" type="tel" name="phone" placeholder="Mobile number" />
                    <textarea class="input-border-bottom border-color-dark-white-transparent bg-transparent extra-large-input px-0 margin-35px-bottom border-radius-0px" name="comment" rows="5" placeholder="How can we help you?"></textarea>
                    <input type="hidden" name="redirect" value="">
                    <button class="btn btn-small btn-fancy btn-gradient-sky-blue-pink mb-0 submit" type="submit">Enviar</button>
                    <div class="form-results d-none"></div>
                </form>
            </div>
        </div>
    </div>
</section>
