<?php  include 'inc/header.php';  ?>

    <div class="main-content">
        <!-- start banner section -->
        <section  class="parallax overflow-visible p-0 md-overflow-hidden" data-parallax-background-ratio="0.3" >            
            <div class="litho-parallax-bg position-absolute cover-background top-0px right-0px w-45">
                <img class="w-100" src="images/litho-landing-page-img-01.png" alt=""/>
            </div>
            <div class="container position-relative z-index-2">
                <div class="row">
                    <div class="col-12 col-xl-6 col-lg-7 col-md-10 d-flex flex-column justify-content-center full-screen position-relative z-index-1 lg-h-700px sm-h-500px">
                        <div class="alt-font text-large font-weight-600 text-uppercase margin-40px-bottom media sm-margin-25px-bottom">
                            <span class="w-30px h-1px bg-fast-blue opacity-7 align-self-center margin-15px-right"></span>
                            <div class="media-body"><span class="text-gradient-sky-blue-dark-pink letter-spacing-minus-1-half">Truly Handcrafted</span></div>
                        </div>
                        <h2 class="alt-font text-extra-dark-gray letter-spacing-minus-2px font-weight-600 margin-10px-bottom sm-margin-5px-bottom">Create anything</h2>
                        <h3 class="alt-font text-extra-dark-gray letter-spacing-minus-2px margin-40px-bottom sm-margin-25px-bottom">that you can imagine</h3>
                        <span class="text-extra-large line-height-36px w-80 xs-w-100">Let your creativity shine and start building your website today and impress your visitors.</span>
                        <div class="position-absolute bottom-100px left-15px sm-bottom-50px">
                            <a href="https://www.youtube.com/watch?v=g0f_BRYJLJE" class="video-icon-box video-icon-medium position-relative popup-youtube">
                                <span>
                                    <span class="video-icon bg-white box-shadow-double-large margin-20px-right">
                                        <i class="fas fa-play text-gradient-sky-blue-pink-2"></i>
                                        <span class="video-icon-sonar">
                                        </span>
                                    </span>
                                    <span class="video-icon-text alt-font text-large text-extra-dark-gray letter-spacing-minus-1-half text-uppercase d-inline-block align-middle font-weight-500"><span class="font-weight-700">Explore</span> litho</span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end banner section -->
        <!-- start section -->
        <section class="position-relative overflow-visible border-bottom border-color-medium-gray">
            <div class="overlap-section z-index-1 d-none d-md-block"><img class="md-w-20" src="images/litho-landing-page-img-02.png" alt="" /></div>
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-12 col-xl-5 col-md-10 text-center text-xl-left lg-margin-5-rem-bottom sm-margin-3-rem-bottom wow animate__fadeIn">
                        <h5 class="alt-font font-weight-500 text-extra-dark-gray letter-spacing-minus-1px mb-0 d-inline-block lg-w-60 md-w-70 xs-w-100">Created <span class="font-weight-600">many concepts</span> with modern and minimal design</h5>
                    </div>
                    <div class="col-12 col-lg-1 d-none d-xl-block wow animate__fadeIn" data-wow-delay="0.2s">
                        <span class="title-large-2 alt-font font-weight-300 opacity-3">}</span>
                    </div>
                    <div class="col-12 col-xl-3 col-lg-4 col-md-5 col-sm-6 xs-margin-20px-bottom wow animate__fadeIn" data-wow-delay="0.4s">
                        <div class="d-flex flex-row align-items-center justify-content-center">
                            <div class="d-flex flex-row align-item-center justify-content-center justify-content-md-start">
                                <h2 class="vertical-counter d-inline-flex text-extra-dark-gray alt-font appear font-weight-600 letter-spacing-minus-3px mb-0 sm-letter-spacing-minus-1-half" data-to="200"></h2><span class="text-extra-dark-gray title-extra-small font-weight-500 margin-5px-left align-self-center">+</span>
                            </div>
                            <span class="alt-font text-large padding-25px-left w-50 lg-w-45 xs-w-auto">Ready elements</span>
                        </div>
                    </div>
                    <div class="col-12 col-xl-3 col-lg-4 col-md-5 col-sm-6 wow animate__fadeIn" data-wow-delay="0.6s">
                        <div class="d-flex flex-row align-items-center justify-content-center">
                            <div class="d-flex flex-row align-item-center justify-content-center justify-content-md-start">
                                <h2 class="vertical-counter d-inline-flex text-extra-dark-gray alt-font appear font-weight-600 letter-spacing-minus-3px mb-0 sm-letter-spacing-minus-1-half" data-to="230"></h2><span class="text-extra-dark-gray title-extra-small font-weight-500 margin-5px-left align-self-center">+</span>
                            </div>
                            <span class="alt-font text-large padding-25px-left w-50 lg-w-45 xs-w-auto">Pre-made templates </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end section -->
        <!-- start section -->
        <section class="big-section bg-gradient-solitude-white pb-0" id="demos">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-10 col-xl-4 col-lg-5 col-sm-6 text-center margin-5-half-rem-bottom md-margin-3-rem-bottom wow animate__fadeIn" data-wow-delay="0.2s">
                        <span class="alt-font font-weight-600 text-extra-medium text-gradient-sky-blue-dark-pink text-uppercase letter-spacing-minus-1-half d-inline-block margin-15px-bottom sm-margin-10px-bottom">Truly multipurpose</span>
                        <h4 class="alt-font font-weight-500 text-extra-dark-gray letter-spacing-minus-2px xs-letter-spacing-minus-1-half">36+ stunning and unique demos</h4>
                    </div>
                </div>
            </div>
        </section>
        <!-- end section -->
        <!-- start section -->
        <section class="pt-0 bg-solitude" id="loja">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center wow animate__fadeIn" data-wow-delay="0.2s">
                        <h2>Lojas</h2>
                    </div>
                </div>
            </div>
            <div class="container-fluid padding-seven-lr xl-padding-four-lr lg-padding-two-lr sm-padding-15px-lr">
                <div class="row loja_lista">
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row novidade_lista">

            </div>
        </div>
        <!-- end section -->
        <!-- start section -->
        <!-- end section -->
        <!-- start section -->
        <!-- end section -->
        <!-- start section -->

        <!-- end section -->
        <!-- start section -->
       
        <!-- end section -->
        <!-- start section -->
        <section class="overlap-height">
            <div class="container overlap-gap-section">
                <div class="row justify-content-center">
                    <div class="col-12 col-xl-4 col-lg-5 col-sm-6 text-center margin-5-rem-bottom md-margin-3-rem-bottom wow animate__fadeIn">
                        <span class="alt-font font-weight-600 text-extra-medium text-gradient-sky-blue-dark-pink text-uppercase d-inline-block letter-spacing-minus-1-half margin-15px-bottom sm-margin-10px-bottom">Alguns dos nossos clientes</span>
                        <h4 class="alt-font font-weight-500 text-extra-dark-gray letter-spacing-minus-2px xs-letter-spacing-minus-1-half">Evolua como eles fizeram </h4>
                    </div>
                </div>
                <div class="row row-cols-1 row-cols-xl-5 row-cols-lg-4 row-cols-md-3 row-cols-sm-2 justify-content-center xs-no-margin-lr">
                    <div class="col text-center padding-40px-tb box-shadow-double-large-hover border-radius-6px transition wow animate__fadeIn" data-wow-delay="0.1s">
                        <img class="margin-25px-bottom" src="images/litho-landing-page-icon-01.jpg" alt=""/>
                        <span class="text-extra-medium font-weight-500 text-extra-dark-gray d-block w-45 mx-auto xs-w-100">Bootstrap 4 Framework</span>
                    </div>
                    <div class="col text-center padding-40px-tb box-shadow-double-large-hover border-radius-6px transition wow animate__fadeIn" data-wow-delay="0.2s">
                        <img class="margin-25px-bottom" src="images/litho-landing-page-icon-02.jpg" alt=""/>
                        <span class="text-extra-medium font-weight-500 text-extra-dark-gray d-block w-45 mx-auto xs-w-100">Revolution Slider</span>
                    </div>
                    <div class="col text-center padding-40px-tb box-shadow-double-large-hover border-radius-6px transition wow animate__fadeIn" data-wow-delay="0.3s">
                        <img class="margin-25px-bottom" src="images/litho-landing-page-icon-03.jpg" alt=""/>
                        <span class="text-extra-medium font-weight-500 text-extra-dark-gray d-block w-45 mx-auto xs-w-100">Google Web Fonts</span>
                    </div>
                    <div class="col text-center padding-40px-tb box-shadow-double-large-hover border-radius-6px transition wow animate__fadeIn" data-wow-delay="0.4s">
                        <img class="margin-25px-bottom" src="images/litho-landing-page-icon-04.jpg" alt=""/>
                        <span class="text-extra-medium font-weight-500 text-extra-dark-gray d-block w-45 mx-auto xs-w-100">Instagram jQuery</span>
                    </div>
                    <div class="col text-center padding-40px-tb box-shadow-double-large-hover border-radius-6px transition wow animate__fadeIn" data-wow-delay="0.5s">
                        <img class="margin-25px-bottom" src="images/litho-landing-page-icon-05.jpg" alt=""/>
                        <span class="text-extra-medium font-weight-500 text-extra-dark-gray d-block w-45 mx-auto xs-w-100">Mailchimp Compatible</span>
                    </div>
                    <div class="col text-center padding-40px-tb box-shadow-double-large-hover border-radius-6px transition wow animate__fadeIn" data-wow-delay="0.5s">
                        <img class="margin-25px-bottom" src="images/litho-landing-page-icon-06.jpg" alt=""/>
                        <span class="text-extra-medium font-weight-500 text-extra-dark-gray d-block w-45 mx-auto xs-w-100">W3C Validation</span>
                    </div>
                    <div class="col text-center padding-40px-tb box-shadow-double-large-hover border-radius-6px transition wow animate__fadeIn" data-wow-delay="0.4s">
                        <img class="margin-25px-bottom" src="images/litho-landing-page-icon-07.jpg" alt=""/>
                        <span class="text-extra-medium font-weight-500 text-extra-dark-gray d-block w-45 mx-auto xs-w-100">Isotope Filtering</span>
                    </div>
                    <div class="col text-center padding-40px-tb box-shadow-double-large-hover border-radius-6px transition wow animate__fadeIn" data-wow-delay="0.3s">
                        <img class="margin-25px-bottom" src="images/litho-landing-page-icon-08.jpg" alt=""/>
                        <span class="text-extra-medium font-weight-500 text-extra-dark-gray d-block w-45 mx-auto xs-w-100">Swiper Touch Slider</span>
                    </div>
                    <div class="col text-center padding-40px-tb box-shadow-double-large-hover border-radius-6px transition wow animate__fadeIn" data-wow-delay="0.2s">
                        <img class="margin-25px-bottom" src="images/litho-landing-page-icon-09.jpg" alt=""/>
                        <span class="text-extra-medium font-weight-500 text-extra-dark-gray d-block w-45 mx-auto xs-w-100">Ajax Contact Form</span>
                    </div>
                    <div class="col text-center padding-40px-tb box-shadow-double-large-hover border-radius-6px transition wow animate__fadeIn" data-wow-delay="0.1s">
                        <img class="margin-25px-bottom" src="images/litho-landing-page-icon-10.jpg" alt=""/>
                        <span class="text-extra-medium font-weight-500 text-extra-dark-gray d-block w-45 mx-auto xs-w-100">Magnific Popup</span>
                    </div>                    
                </div>
            </div>
        </section>
        <!-- end section -->
        <!-- start section -->
        <!-- end section -->
        <!-- start section -->
      
        <!-- end section -->
        <!-- start section -->
       
        <!-- end section -->
        <!-- start section -->
        <section class="overflow-visible pt-md-0 pb-0 d-none d-md-block">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center overlap-section wow animate__zoomIn" data-wow-delay="0.2s">
                        <span class="alt-font font-weight-600 d-inline-block text-big text-extra-dark-gray letter-spacing-minus-5px">ecommerce</span>
                    </div>
                </div>
            </div>
        </section>
        <!-- end section -->
        <!-- start section -->
        <section>
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-12 col-lg-4 col-md-8 text-center text-lg-left md-margin-5-rem-bottom wow animate__fadeIn" data-wow-delay="0.6s">
                        <span class="alt-font font-weight-600 text-extra-medium text-gradient-sky-blue-dark-pink text-uppercase d-inline-block letter-spacing-minus-1-half margin-20px-bottom sm-margin-10px-bottom">eCommerce ready</span>
                        <h4 class="alt-font font-weight-500 text-extra-dark-gray letter-spacing-minus-2px xs-letter-spacing-minus-1-half w-95 margin-3-half-rem-bottom lg-w-100 sm-margin-2-half-rem-bottom">Compatible for any eCommerce platform</h4>
                        <p class="text-extra-medium line-height-32px w-90 mb-0 lg-w-100">Present your products online by grabbing public attention and boost revenue with Litho. Kick start your eCommerce site using modern looking eCommerce designs.</p>
                        <a href="home-fashion-shop.html" target="_blank" class="btn btn-fancy btn-large btn-dark-gray margin-4-rem-top">Discover demo</a>
                    </div>
                    <div class="col-12 col-lg-8 wow animate__fadeIn" data-wow-delay="0.2s">
                        <div class="outside-box-right">
                            <img src="images/litho-landing-page-img-05.jpg" alt=""/>
                        </div>
                    </div>
                </div>
                <div class="row row-cols-1 row-cols-lg-4 row-cols-sm-2 margin-9-rem-top margin-2-rem-bottom">
                    <div class="col text-center md-margin-5-rem-bottom wow animate__fadeInUp" data-wow-delay="0.2s">
                        <div><img class="margin-35px-bottom border-radius-100 border-all border-width-10px border-color-white box-shadow-double-large xs-margin-20px-bottom" src="images/litho-landing-page-img-06.jpg" alt=""/></div>
                        <span class="text-large line-height-26px font-weight-500 text-extra-dark-gray d-inline-block w-35 lg-w-45 md-w-100">Appealing hover style</span>
                    </div>
                    <div class="col text-center md-margin-5-rem-bottom wow animate__fadeInUp" data-wow-delay="0.4s">
                        <div><img class="margin-35px-bottom border-radius-100 border-all border-width-10px border-color-white box-shadow-double-large xs-margin-20px-bottom" src="images/litho-landing-page-img-07.jpg" alt=""/></div>
                        <span class="text-large line-height-26px font-weight-500 text-extra-dark-gray d-inline-block w-35 lg-w-45 md-w-100">Detailed filtering</span>
                    </div>
                    <div class="col text-center xs-margin-5-rem-bottom wow animate__fadeInUp" data-wow-delay="0.6s">
                        <div><img class="margin-35px-bottom border-radius-100 border-all border-width-10px border-color-white box-shadow-double-large xs-margin-20px-bottom" src="images/litho-landing-page-img-08.jpg" alt=""/></div>
                        <span class="text-large line-height-26px font-weight-500 text-extra-dark-gray d-inline-block w-35 lg-w-45 md-w-100">Modern mini cart</span>
                    </div>
                    <div class="col text-center wow animate__fadeInUp" data-wow-delay="0.8s">
                        <div><img class="margin-35px-bottom border-radius-100 border-all border-width-10px border-color-white box-shadow-double-large xs-margin-20px-bottom" src="images/litho-landing-page-img-09.jpg" alt=""/></div>
                        <span class="text-large line-height-26px font-weight-500 text-extra-dark-gray d-inline-block w-35 lg-w-45 md-w-100">Attractive gallery</span>
                    </div>
                </div>
            </div>
        </section>
        <!-- end section -->
        <!-- start section -->
        <section id="features" class="bg-solitude">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-5 col-sm-6 text-center margin-5-half-rem-bottom md-margin-3-rem-bottom wow animate__fadeIn">
                        <span class="alt-font font-weight-600 text-extra-medium text-gradient-sky-blue-dark-pink text-uppercase d-inline-block letter-spacing-minus-1-half margin-15px-bottom sm-margin-10px-bottom">Oque procura?</span>
                        <h4 class="alt-font font-weight-500 text-extra-dark-gray letter-spacing-minus-2px xs-letter-spacing-minus-1-half">Confira nossa categorias</h4>
                    </div>
                </div>
                <div class="row row-cols-1 row-cols-xl-6 row-cols-lg-5 row-cols-sm-3 justify-content-center">
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.1s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-target icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Animations</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.5s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-bell icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Alert Boxes</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.8s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-navigation icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Buttons</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.2s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-message-square icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Blockquotes</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.9s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-arrow-right-circle icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Call To Action</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.3s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-layers icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Carousel</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.6s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-pie-chart icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Pie Charts</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.1s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-user icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Clients</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.5s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-sidebar icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Column Grids</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.9s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-mail icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Newsletter</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.4s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-calendar icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Event Tab</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.8s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-help-circle icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">FAQs</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.3s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-box icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Flip Box</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.5s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-server icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Forms</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.8s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-image icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Galleries</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.1s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-book-open icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Info Boxes</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.5s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-maximize icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Lightbox</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.9s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-map-pin icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Maps</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.7s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-video icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Media Embeds</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.3s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-external-link icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Modals</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.2s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-menu icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Accordion</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.4s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-maximize-2 icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Parallax</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.8s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-folder icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Mega Menu</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.1s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-align-left icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Progress Bars</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.9s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-percent icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Promo Boxes</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.6s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-dollar-sign icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Pricing Tables</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.4s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-repeat icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Process Steps</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.3s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-grid icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Portfolio</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.9s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-star icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Ratings</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.7s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-tablet icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Responsive</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.4s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-sidebar icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Sidebar</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.2s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-shopping-cart icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">e-Commerce</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.9s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-globe icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Social Icons</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.3s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-sidebar icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Tables</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.6s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-type icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Text Rotator</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.7s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-credit-card icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Tabs</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col md-margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.2s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-message-circle icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Testimonials</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col md-margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.4s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-eye icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Retina Ready</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col md-margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.8s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-search icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">SEO Optimized</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.1s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-filter icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Unlimited Colors</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col xs-margin-15px-bottom wow animate__fadeIn" data-wow-delay="0.2s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-edit icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Documentation</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col wow animate__fadeIn" data-wow-delay="0.5s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-shadow box-shadow-large feature-box-dark-hover overflow-hidden padding-3-rem-tb bg-white border-radius-4px">
                            <div class="feature-box-icon">
                                <i class="feather icon-feather-rotate-cw icon-extra-medium text-echo-blue d-inline-block margin-25px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="text-extra-medium font-weight-500 d-block text-extra-dark-gray">Free Updates</span>
                            </div>
                            <div class="feature-box-overlay bg-east-bay"></div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                </div>
            </div>
        </section>
        <!-- end section -->
        <!-- start section -->
        
        <!-- end section -->
        <!-- start section -->
        <section class="position-relative big-section">
            <img class="customer-bg-section lg-w-20 wow animate__fadeIn" data-wow-delay="0.6s" src="images/litho-landing-page-img-13.jpg" alt=""/>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-8 text-center margin-9-half-rem-bottom md-margin-2-half-rem-bottom wow animate__fadeIn" data-wow-delay="0.2s">
                        <h2 class="alt-font text-extra-dark-gray letter-spacing-minus-2px mb-0"><span class="font-weight-600">3300+</span> Projetos customizados</h2>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12 col-xl-4 col-lg-5 col-md-10 text-center text-lg-left position-relative md-margin-3-rem-bottom wow animate__fadeIn" data-wow-delay="0.4s">
                        <h4 class="alt-font font-weight-500 text-extra-dark-gray letter-spacing-minus-2px xs-letter-spacing-minus-1-half margin-2-rem-bottom md-margin-1-rem-bottom lg-w-90 md-w-100">Thousands of <span class="font-weight-600">happy customers</span></h4>
                        <h6 class="font-weight-300 margin-40px-bottom letter-spacing-minus-1-half xs-margin-25px-bottom"><span class="d-lg-block d-inline-block">Trusted and experienced</span> <span class="font-weight-500 text-decoration-line-bottom text-violet">Power elite author</span></h6>
                        <div class="swiper-button-next-nav-3 swiper-button-next slider-navigation-style-03 rounded-circle d-none d-lg-flex"><i class="fa fa-arrow-right text-medium-gray text-medium"></i></div>
                        <div class="swiper-button-previous-nav-3 swiper-button-prev slider-navigation-style-03 rounded-circle d-none d-lg-flex"><i class="fa fa-arrow-left text-medium-gray text-medium"></i></div>
                    </div>
                    <div class="col-12 col-lg-7 offset-xl-1 wow animate__fadeIn" data-wow-delay="0.7s">
                        <div class="testimonials-carousel-style-01 swiper-simple-arrow-style-1 wow animate__fadeIn">
                            <div class="swiper-container" data-slider-options='{ "loop": true, "slidesPerView": 1, "spaceBetween": 30, "observer": true, "observeParents": true, "pagination": { "el": ".slider-three-slide-pagination-1", "clickable": true, "dynamicBullets": true }, "navigation": { "nextEl": ".swiper-button-next-nav-3", "prevEl": ".swiper-button-previous-nav-3" }, "keyboard": { "enabled": true, "onlyInViewport": true }, "breakpoints": { "992": { "slidesPerView": 3 }, "768": { "slidesPerView": 2 } }, "effect": "slide" }'>
                                <div class="swiper-wrapper black-move">
                                    <!-- start testimonials item -->
                                    <div class="swiper-slide text-center">
                                        <div class="feature-box feature-box-left-icon-middle padding-3-rem-tb padding-3-half-rem-lr bg-white border-all border-color-medium-gray border-radius-6px last-paragraph-no-margin">
                                            <div class="feature-box-icon margin-25px-right">
                                                <img class="rounded-circle w-65px h-65px" src="images/litho-landing-page-img-12.jpg" alt="Herman Miller"/>
                                            </div>
                                            <div class="feature-box-content">
                                                <div class="margin-10px-bottom text-small text-golden-yellow">
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                </div>
                                                <div class="text-extra-dark-gray text-extra-medium alt-font font-weight-500 line-height-12px">Adorn Themes</div>
                                            </div>
                                            <p class="margin-30px-top">Every element is designed beautifully and pixel perfect, so it is really a modern theme. Support team is very kind.</p>
                                        </div>
                                    </div>
                                    <!-- end testimonials item -->
                                    <!-- start testimonials item -->
                                    <div class="swiper-slide text-center">
                                        <div class="feature-box feature-box-left-icon-middle padding-3-rem-tb padding-3-half-rem-lr bg-white border-all border-color-medium-gray border-radius-6px last-paragraph-no-margin">
                                            <div class="feature-box-icon margin-20px-right">
                                                <img class="rounded-circle w-65px h-65px" src="images/litho-landing-page-img-12.jpg" alt="Jeremy Girard"/>
                                            </div>
                                            <div class="feature-box-content">
                                                <div class="margin-10px-bottom text-small text-golden-yellow">
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                </div>
                                                <div class="text-extra-dark-gray text-extra-medium alt-font font-weight-500 line-height-12px">Ben AVW</div>
                                            </div>
                                            <p class="margin-30px-top">You don't need a web developer to use this theme. Simple and easy to integrate and build the whole website.</p>
                                        </div>
                                    </div>
                                    <!-- end testimonials item -->
                                    <!-- start testimonials item -->
                                    <div class="swiper-slide text-center">
                                        <div class="feature-box feature-box-left-icon-middle padding-3-rem-tb padding-3-half-rem-lr bg-white border-all border-color-medium-gray border-radius-6px last-paragraph-no-margin">
                                            <div class="feature-box-icon margin-20px-right">
                                                <img class="rounded-circle w-65px h-65px" src="images/litho-landing-page-img-12.jpg" alt="Jeremy Girard"/>
                                            </div>
                                            <div class="feature-box-content">
                                                <div class="margin-10px-bottom text-small text-golden-yellow">
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                </div>
                                                <div class="text-extra-dark-gray text-extra-medium alt-font font-weight-500 line-height-12px">Chantal Winston</div>
                                            </div>
                                            <p class="margin-30px-top">Out of all the themes that I have purchased and customized, this is the 1st one that didn't require any support.</p>
                                        </div>
                                    </div>
                                    <!-- end testimonials item -->
                                    <!-- start testimonials item -->
                                    <div class="swiper-slide text-center">
                                        <div class="feature-box feature-box-left-icon-middle padding-3-rem-tb padding-3-half-rem-lr bg-white border-all border-color-medium-gray border-radius-6px last-paragraph-no-margin">
                                            <div class="feature-box-icon margin-20px-right">
                                                <img class="rounded-circle w-65px h-65px" src="images/litho-landing-page-img-12.jpg" alt="Jeremy Girard"/>
                                            </div>
                                            <div class="feature-box-content">
                                                <div class="margin-10px-bottom text-small text-golden-yellow">
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                </div>
                                                <div class="text-extra-dark-gray text-extra-medium alt-font font-weight-500 line-height-12px">Studio JAS</div>
                                            </div>
                                            <p class="margin-30px-top">While everything is great about this theme, from design quality to flexibility. I'm using this theme for 5 websites.</p>
                                        </div>
                                    </div>
                                    <!-- end testimonials item -->
                                    <!-- start testimonials item -->
                                    <div class="swiper-slide text-center">
                                        <div class="feature-box feature-box-left-icon-middle padding-3-rem-tb padding-3-half-rem-lr bg-white border-all border-color-medium-gray border-radius-6px last-paragraph-no-margin">
                                            <div class="feature-box-icon margin-20px-right">
                                                <img class="rounded-circle w-65px h-65px" src="images/litho-landing-page-img-12.jpg" alt="Jeremy Girard"/>
                                            </div>
                                            <div class="feature-box-content">
                                                <div class="margin-10px-bottom text-small text-golden-yellow">
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                </div>
                                                <div class="text-extra-dark-gray text-extra-medium alt-font font-weight-500 line-height-12px">Brent Nolan</div>
                                            </div>
                                            <p class="margin-30px-top">Support for this product has always been above and beyond, their technical team has been very reliable.</p>
                                        </div>
                                    </div>
                                    <!-- end testimonials item -->
                                    <!-- start testimonials item -->
                                    <div class="swiper-slide text-center">
                                        <div class="feature-box feature-box-left-icon-middle padding-3-rem-tb padding-3-half-rem-lr bg-white border-all border-color-medium-gray border-radius-6px last-paragraph-no-margin">
                                            <div class="feature-box-icon margin-20px-right">
                                                <img class="rounded-circle w-65px h-65px" src="images/litho-landing-page-img-12.jpg" alt="Jeremy Girard"/>
                                            </div>
                                            <div class="feature-box-content">
                                                <div class="margin-10px-bottom text-small text-golden-yellow">
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                </div>
                                                <div class="text-extra-dark-gray text-extra-medium alt-font font-weight-500 line-height-12px">Forextv</div>
                                            </div>
                                            <p class="margin-30px-top">I have limited coding skills and these guys make it simple. Recommend this theme and support team.</p>
                                        </div>
                                    </div>
                                    <!-- end testimonials item -->
                                    <!-- start testimonials item -->
                                    <div class="swiper-slide text-center">
                                        <div class="feature-box feature-box-left-icon-middle padding-3-rem-tb padding-3-half-rem-lr bg-white border-all border-color-medium-gray border-radius-6px last-paragraph-no-margin">
                                            <div class="feature-box-icon margin-20px-right">
                                                <img class="rounded-circle w-65px h-65px" src="images/litho-landing-page-img-12.jpg" alt="Jeremy Girard"/>
                                            </div>
                                            <div class="feature-box-content">
                                                <div class="margin-10px-bottom text-small text-golden-yellow">
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                    <i class="fas fa-star"></i>
                                                </div>
                                                <div class="text-extra-dark-gray text-extra-medium alt-font font-weight-500 line-height-12px">Super Mario</div>
                                            </div>
                                            <p class="margin-30px-top">A professional template with a great code quality and a very detailed and understandable documentation.</p>
                                        </div>
                                    </div>
                                    <!-- end testimonials item -->
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end section -->
        <!-- start section -->
        <section class="bg-nero-gray">
            <div class="container">
                <div class="row row-cols-1 row-cols-lg-3 row-cols-sm-2 justify-content-center">
                    <div class="col text-center md-margin-5-rem-bottom xs-margin-6-rem-bottom wow animate__fadeInUp" data-wow-delay="0.2s">
                        <i class="line-icon-Business-Man icon-large margin-30px-bottom"></i>
                        <span class="alt-font font-weight-500 text-large text-white d-block margin-5px-bottom">Como podemos ajudar</span>
                        <a href="support.php" target="_blank" class="alt-font font-weight-500 text-uppercase text-medium-gray-hover text-decoration-line-bottom">Suporte Agora</a>
                    </div>
                    <div class="col text-center md-margin-5-rem-bottom xs-margin-6-rem-bottom wow animate__fadeInUp" data-wow-delay="0.4s">
                        <i class="line-icon-Address-Book icon-large margin-30px-bottom"></i>
                        <span class="alt-font font-weight-500 text-large text-white d-block margin-5px-bottom">Sobre Nós</span>
                        <a href="sobre.php" target="_blank" class="alt-font font-weight-500 text-uppercase text-medium-gray-hover text-decoration-line-bottom">Leia sobre nossa história</a>
                    </div>
                    <div class="col text-center wow animate__fadeInUp" data-wow-delay="0.6s">
                        <i class="line-icon-Vector-5 icon-large margin-30px-bottom"></i>
                        <span class="alt-font font-weight-500 text-large text-white d-block margin-5px-bottom">Procurando por customização?</span>
                        <a href="https://www.themezaa.com/theme-customization/" target="_blank" class="alt-font font-weight-500 text-uppercase text-medium-gray-hover text-decoration-line-bottom">Mande seu projeto</a>
                    </div>
                </div>
            </div>
        </section>
        <!-- end section -->
        <?php  include 'contact-form.php';  ?>
        
    </div>
        <?php  include 'inc/footer.php';  ?>