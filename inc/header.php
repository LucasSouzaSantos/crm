<?php 
include 'text.php';  
$pagina= str_replace('.php','',substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1));
?>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <title>Litho – The Multipurpose HTML5 Template</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="author" content="ThemeZaa">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
        <meta name="description" content="Litho is a clean and modern design, BootStrap 4 responsive, business and portfolio multipurpose HTML5 template with 36+ ready homepage demos.">
        <!-- favicon icon -->
        <link rel="shortcut icon" href="images/favicon.png">
        <link rel="apple-touch-icon" href="images/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
        <!-- style sheets and font icons  -->
        <link rel="stylesheet" type="text/css" href="css/font-icons.min.css">
        <link rel="stylesheet" type="text/css" href="css/theme-vendors.min.css">
        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="css/responsive.css" />
    </head>
    <script type="text/javascript">
        var pagina='<?php echo $pagina; ?>';
        var shopping_id=<?php  echo $shopping_id ?>; 
        var shopping_nome="<?php  echo $shopping_nome ?>";
        var shopping_token="<?php  echo $shopping_token ?>";
        var shopping_coordenada=[<?php  echo $shopping_coordenada ?>];
        var novidade_id= "<?php if( isset( $_REQUEST['novidade_id'] ) ) { echo $_REQUEST['novidade_id'];  }  ?>";
        var alerta= "<?php if( isset( $_REQUEST['alerta'] ) ) { echo $_REQUEST['alerta'];  }  ?>";
        var cinema_id= "<?php if( isset( $_REQUEST['cinema_id'] ) ) { echo $_REQUEST['cinema_id'];   }  ?>";
        var loja_id= "<?php if( isset( $_REQUEST['loja_id'] ) ) { echo $_REQUEST['loja_id'];   }  ?>"; 
        var busca= "<?php if( isset( $_REQUEST['busca'] ) ) { echo $_REQUEST['busca'];   }  ?>";
        var audio= "<?php if( isset( $_REQUEST['filtro_audio'] ) ) { echo $_REQUEST['filtro_audio'];   }  ?>";
        var video= "<?php if( isset( $_REQUEST['filtro_video'] ) ) { echo $_REQUEST['filtro_video'];   }  ?>";
        var filtro_dia= "<?php if( isset( $_REQUEST['filtro_dia'] ) ) { echo $_REQUEST['filtro_dia'];   }  ?>";
        var filme= "<?php if( isset( $_REQUEST['filme'] ) ) { echo $_REQUEST['filme'];   }  ?>";

        var _gaq=_gaq||[];
        _gaq.push(['_setAccount','<?php  echo $shopping_analytics ?>']);
        _gaq.push(['_trackPageview']);
        (function(){
            var ga=document.createElement('script');
            ga.type='text/javascript';
            ga.async=true;
            ga.src=('https:'==document.location.protocol?'https://ssl':'http://www')+'.google-analytics.com/ga.js';
            var s=document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga,s);
        })(); 
    </script>
    <body class="landing" data-mobile-nav-style="classic">
        <!-- start header -->
        <header>
            <!-- start navigation -->
            <nav class="navbar navbar-boxed navbar-expand-lg navbar-light bg-transparent header-light position-absolute top-0px left-0px right-0px z-index-3 litho-landing-header litho-demo">
                <div class="container-fluid nav-header-container h-120px md-h-80px xs-h-auto">
                    <div class="col-auto mr-auto mr-lg-0">
                        <a class="navbar-brand padding-20px-right md-no-padding-right" href="index.html">
                            <img src="images/logo-gradient-sky-blue-dark-pink.png" data-at2x="images/logo-gradient-sky-blue-dark-pink@2x.png" class="default-logo" alt="">
                            <img src="images/logo-gradient-sky-blue-dark-pink.png" data-at2x="images/logo-gradient-sky-blue-dark-pink@2x.png" class="mobile-logo" alt="">
                        </a>
                    </div>
                    <div class="col-auto menu-order mr-lg-auto">
                        <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-label="Toggle navigation">
                            <span class="navbar-toggler-line"></span>
                            <span class="navbar-toggler-line"></span>
                            <span class="navbar-toggler-line"></span>
                            <span class="navbar-toggler-line"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav alt-font text-uppercase">
                                <li class="nav-item">
                                    <a href="index.php" class="nav-link section-link">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#loja" class="nav-link section-link">Lojas</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#contato" target="_blank" class="nav-link">Contato</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-auto text-right pr-0 font-size-0 d-none d-sm-block">
                        <div class="header-button">
                            <a href="login.php" target="_blank" class="btn btn-large btn-gradient-sky-blue-pink box-shadow-double-large btn-round-edge-small">Login</a>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- end navigation -->
        </header>
        <!-- end header -->