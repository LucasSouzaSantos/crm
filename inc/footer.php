        <!-- start footer -->
        <footer class="landing-page-footer bg-nero-gray background-position-center-top padding-10-rem-tb md-padding-7-rem-tb footer-sticky" style="background-image: url('images/litho-landing-page-bg-img-01.jpg');">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <span class="alt-font font-weight-600 title-large-2 text-white letter-spacing-minus-2px d-block margin-6-half-rem-bottom text-shadow-double-large md-margin-5-rem-bottom xs-letter-spacing-minus-1-half">Create innovative website today. Purchase Litho template now! </span>
                        <a href="https://themeforest.net/user/themezaa/portfolio" target="_blank" class="btn btn-fancy btn-extra-large btn-round-edge-small btn-gradient-sky-blue-pink margin-10-rem-bottom box-shadow-double-large md-margin-7-rem-bottom xs-margin-4-rem-bottom">Purchase template</a>
                        <div class="d-block text-center margin-20px-top">
                            <span class="alt-font d-block d-sm-inline-block align-middle margin-15px-right xs-no-margin-right">Powered By</span>
                            <a href="https://www.themezaa.com/" target="_blank"><img src="images/themezaa-logo.png" data-at2x="images/themezaa-logo@2x.png" alt=""/></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end footer -->
        <!-- start scroll to top -->
        <a class="scroll-top-arrow" href="javascript:void(0);"><i class="feather icon-feather-arrow-up"></i></a>
        <!-- end scroll to top -->
        <!-- javascript -->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/theme-vendors.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="assets/js/bunner.js"></script>
        <!-- <script type="text/javascript" src="assets/js/lojas.js"></script> -->
        <script src="assets/js/lojas.js?v=1.5"></script>
        <script src="assets/js/novidade.js"></script>
    </body>
</html>