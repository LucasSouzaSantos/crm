$(function(){

	var d = new Date();
  	var year = d.getFullYear();
      var tipo = '5';  

	if (pagina == "novidade" ){
		dir = 'Acontece';
		tipo = '5';
	}
	novidade_lista = function (){
		
		if($('[name="ano"]').val()&&$('[name="mes"]').val()){
			url="https://sal.madnezz.com.br/api/site/json/novidade.asp?tp="+tipo+"&ano="+$("[name='ano']").val()+"&mes="+$("[name='mes']").val()+"&shopping_id="+shopping_id+"&jsoncallback=?"
		}else if($("[name='ano']").val()){
			url="https://sal.madnezz.com.br/api/site/json/novidade.asp?tp="+tipo+"&ano="+$("[name='ano']").val()+"&shopping_id="+shopping_id+"&jsoncallback=?"
		}else{
			url="https://sal.madnezz.com.br/api/site/json/novidade.asp?tp="+tipo+"&ano="+year+"&shopping_id="+shopping_id+"&jsoncallback=?"
		}
		$.ajax({url:url,dataType:"json"}).done(function(data){	
            console.log(data);			
			if(data.length){									
				$(".novidade_lista").html('');							
				$.each(data,function(i,novidade){	
					if (novidade.novidade_imagem_1) {
						novidade_img = 'https://sal.madnezz.com.br/api/site/upload/'+dir+'/'+novidade.novidade_imagem_1;
					} else{
						novidade_img = 'img/novidade_padrao.jpg';
					};

                    var meses = ["", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Augusto", "Setembro", "Outubro", "Novembro", "Dezembro"];
					var dia = novidade.novidade_entrada.substring(0,2);
					var mes = parseInt(novidade.novidade_entrada.substring(3,5));
					var ano = novidade.novidade_entrada.substring(6,10);
					var mesEscrito = meses[mes];

					$('.novidade_lista').append(
						'<div class="col-md-6 col-lg-4">'+
                            '<a href="novidade.php?novidade_id='+novidade.novidade_id+'">'+
                                '<div class="post-item matchHeight">'+
                                    '<span class="post-day">'+dia+'</span>'+
                                    '<div class="post-thumb" style="background-image:url('+novidade_img+');"></div>'+
                                    '<div class="post-info">'+
                                        '<h4 class="post-info__title">'+novidade.novidade_nome+'</h4>'+
                                        '<div class="post-info__meta">'+dia+' '+mesEscrito+', '+ano+'</div>'+
                                        '<div class="post-info__excerpts">'+
                                            '<p>'+(novidade.novidade_texto.length>120?novidade.novidade_texto.substring(0,120)+'...':novidade.novidade_texto)+'</p>'+
                                        '</div>'+
                                        '<div class="post-info__action">'+
                                            '<span class="btn btn-bottom">Saiba mais</span>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</a>'+
                        '</div>'
					);	
				});
		
				if(novidade_id){						
					$('#novidade_busca,#novidade_lista').hide();
                    $('#novidade_info').show();
                    $('.blog-page-content-area').addClass('bg-softWhite');
					novidade_info(novidade_id);
				}
				$('.loading').hide();
				if(data.length){
					$(".novidade_qtd").hide();
				}
				
			}else{
				$('.novidade_lista').html("");
				$(".novidade_qtd").show();
				$(".novidade_qtd").html("Nenhuma ocorrência encontrada!");
			} 

			//FILTROS
			$('.blog-wrapper').each( function() {
				var _this = $( this );
				if( ! _this.find( '.wow' ).length > 0 ) {
					_this.find( '.grid-item' ).css( 'visibility', 'hidden' );
				}
				_this.imagesLoaded( function () {
					if( ! _this.find( '.wow' ).length > 0 ) {
						_this.find( '.grid-item' ).css( 'visibility', '' );
					} else if( ! isMobile ) {
						_this.find( '.grid-item' ).css( 'visibility', 'hidden' );
					}
					_this.removeClass( 'grid-loading' );
					_this.isotope({
						layoutMode: 'masonry',
						itemSelector: '.grid-item',
						percentPosition: true,
						stagger: 0,
						masonry: {
							columnWidth: '.grid-sizer',
						}
					});
					isotopeObjs.push( _this );
				});
			});

			$( document ).on( 'click', '.blog-filter > li > a', function () {
				var _this           = $( this ),
					parentSectionObj= _this.parents( 'section' );
				parentSectionObj.find( '.blog-filter > li' ).removeClass( 'active' );
				_this.parent().addClass( 'active' );
				var selector    = _this.attr( 'data-filter' ),
					blogFilter  = parentSectionObj.find( '.blog-wrapper' );
				blogFilter.isotope({ filter: selector });
				return false;
				
			});
		});	
	}
	
	novidade_info = function (novidade_id){ 
		$('.busca_novidade').hide();
		$('.novidade_qtd').hide();

		$.ajax({url:"https://sal.madnezz.com.br/api/site/json/novidade.asp?novidade_id="+novidade_id+"&tp="+tipo+"&shopping_id="+shopping_id+"&jsoncallback=?",dataType:"json"}).done(function(data){
			_gaq.push(["_trackPageview","novidade.asp?click="+data[0].novidade_nome]);
			
			if (data[0].novidade_imagem_1) {
				novidade_img = 'https://sal.madnezz.com.br/api/site/upload/'+dir+'/'+data[0].novidade_imagem_1;
			} else{
				novidade_img = 'img/novidade_padrao.jpg';
			};

            var meses = ["", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Augusto", "Setembro", "Outubro", "Novembro", "Dezembro"];
            var dia = data[0].novidade_entrada.substring(0,2);
            var mes = parseInt(data[0].novidade_entrada.substring(3,5));
            var ano = data[0].novidade_entrada.substring(6,10);
            var mesEscrito = meses[mes];

			$('.novidade_info .col-lg-8').html(			
                '<article class="blog-post-details-content">'+
                    '<header class="blog-post-details-header">'+
                        '<div class="post-info text-center">'+
                            '<h4 class="post-info__title">'+data[0].novidade_nome+'</h4>'+
                            '<div class="post-info__meta">'+dia+' '+mesEscrito+', '+ano+'</div>'+
                        '</div>'+
                    '</header>'+

                    '<div class="blog-post-details-body">'+
                        '<figure class="post-thumbnail">'+
                            '<img src="'+novidade_img+'" alt="'+data[0].novidade_nome+'" />'+
                        '</figure>'+

                        '<div class="post-content">'+
                            '<p>'+data[0].novidade_texto+'</p>'+
                        '</div>'+
                    '</div>'+
                '</article>'+

                '<div class="form-input-item mb-4 mt-5">'+
                    '<a href="?"><button class="btn btn-bordered w-auto px-5">Voltar</button></a>'+
                '</div>'
			);				
		});		
        
        url="https://sal.madnezz.com.br/api/site/json/novidade.asp?tp="+tipo+"&ano="+year+"&shopping_id="+shopping_id+"&jsoncallback=?"
		$.ajax({url:url,dataType:"json"}).done(function(data){	
            console.log(data);			
			if(data.length){									
				$(".novidade_lista").html('');							
				$.each(data,function(i,novidade){	
                    if(novidade.novidade_id != novidade_id){
                        if (novidade.novidade_imagem_1) {
                            novidade_img = 'https://sal.madnezz.com.br/api/site/upload/'+dir+'/'+novidade.novidade_imagem_1;
                        } else{
                            novidade_img = 'img/novidade_padrao.jpg';
                        };

                        var meses = ["", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Augusto", "Setembro", "Outubro", "Novembro", "Dezembro"];
                        var dia = novidade.novidade_entrada.substring(0,2);
                        var mes = parseInt(novidade.novidade_entrada.substring(3,5));
                        var ano = novidade.novidade_entrada.substring(6,10);
                        var mesEscrito = meses[mes];

                        $('.novidade_info .col-lg-4 .related-post-area').append(
                            '<div class="post-item mb-4">'+
                                '<a href="novidade.php?novidade_id='+novidade.novidade_id+'">'+
                                    '<span class="post-day">'+dia+'</span>'+
                                    '<div class="post-thumb" style="background-image:url('+novidade_img+');"></div>'+
                                    '<div class="post-info">'+
                                        '<h4 class="post-info__title">'+novidade.novidade_nome+'</a></h4>'+
                                        '<div class="post-info__meta">'+dia+' '+mesEscrito+', '+ano+'</div>'+
                                    '</div>'+
                                '</a>'+
                            '</div>'
                        );	
                    }
				});
            }
        });
	}
		
	novidade_lista(); 	

    //BUSCA POR DATA
    $('[name="ano"]').change(function(){
        novidade_lista();
    });

    //BUSCA POR MÊS
    $('[name="mes"]').change(function(){
        novidade_lista();
    });	
});