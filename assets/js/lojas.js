$(function(){	
	var imagem_padrao;
	var swiper;
    var tipo = 1;
	if(pagina=='loja'){
		tipo="1";
		link="loja";
		nomePagina="Lojas";
	} else if(pagina=='gourmet'){
		tipo="2,5";
		link="alimentacao";
		nomePagina="Alimentação";
	} else if(pagina=='alimentacao'){
		tipo="2,5";
		link="alimentacao";
		nomePagina="Alimentação";
	} else if(pagina=='servico'){
		tipo="3";
		link="servico";
		nomePagina="Serviços";
	}
	 
	function monta_loja(){
		
		$.ajax({url:"https://sal.madnezz.com.br/api/site/json/loja.asp?tipo="+tipo+"&shopping_id="+shopping_id+"&filtro_loja="+loja_id+"&full=true&jsoncallback=?",dataType:"json"}).done(function(data){
			imagem = (data.loja_imagem_1?'https://sal.madnezz.com.br/api/site/upload/loja/'+data.loja_imagem_1:'img/loja-padrao.jpg');
			logo = (data.loja_logo?'https://sal.madnezz.com.br/api/site/upload/loja/'+data.loja_logo:'img/logo-padrao.jpg');

			$('.breadcrumb').find('.active').html('<a href="'+link+'.php"><span>'+nomePagina+'</span></a>').removeClass('active');
			$('.breadcrumb').append('<li class="breadcrumb-item active"><span>'+data.loja_nome+'</span></li>');

						
			$('.loja_lista').html(	
				'<div class="lqd-column col-md-6">'+
					'<h2 class="mt-2 mb-3 font-weight-normal lh-1">'+data.loja_nome+'</h2>	'+
					'<p class="h3 font-weight-light font-size-30 pr-7 mr-7 mb-40"><i>'+data.ramo_nome+'</i></p>	'+
					'<p class="pr-7 mr-7 mb-60 font-size-18 lh-175">'+data.loja_texto+'</p>'+
					(data.loja_telefone?'<a href="tel:'+data.loja_telefone+'" class="btn btn-solid text-uppercase btn-md circle btn-bordered border-thin font-size-14 font-weight-bold ltr-sp-025 lh-15 px-1">'+
						'<span>'+
							'<span class="btn-txt"><i class="fas fa-phone pr-2"></i> Ligar</span>'+
						'</span>'+
					'</a>':'')+
				'</div>	'+
				'<div class="lqd-column col-md-5 col-md-offset-1">	'+
					'<div class="lqd-parallax-images">	'+
						'<div class="liquid-img-group-single" data-shadow-style="4" data-inview="true" data-animate-shadow="true">'+
							'<div class="liquid-img-group-img-container">'+
								'<div class="liquid-img-container-inner">'+
									'<figure>'+
										'<img src="'+imagem+'" alt="Good Design" style="width:430px;height:548px;"/>'+
									'</figure>'+
								'</div>'+
							'</div>'+
						'</div>								'+
						'<div class="liquid-img-group-single w-75" data-shadow-style="4" data-inview="true" data-animate-shadow="true">'+
							'<div class="liquid-img-group-img-container">'+
								'<div class="liquid-img-container-inner">'+
									'<figure>'+
										'<img src="'+logo+'" alt="Good Design" style="width:345px;height:372px;" />'+
									'</figure>'+
								'</div>'+
							'</div>'+
						'</div>	'+
					'</div>	'+
				'</div>'
			);
			_gaq.push(["_trackPageview","loja.php?filtro_loja="+data.loja_nome]);
			
			
			
		}); 
	}

	loja_lista = function(loja){
		logo = (loja.loja_logo?'https://sal.madnezz.com.br/api/site/upload/loja/'+loja.loja_logo:'img/logo-padrao.jpg');
		imagem = (loja.loja_imagem_1?'https://sal.madnezz.com.br/api/site/upload/loja/'+loja.loja_imagem_1:'img/loja-padrao.jpg');		
		$(".loja_lista").append(
			'<div class="col-md-6 col-lg-4">'+
                '<div class="pricing-item d-flex flex-column justify-content-center align-items-center">'+
                    '<div class="pricing-item-header">'+
                        (loja.loja_logo?'<div class="price-wrap" style="background-image:url('+logo+');"></div>':'<div class="price-wrap" style="background-image:url(img/logo-pb.png);"></div>')+
                        '<h2 class="h4">'+loja.loja_nome.toLowerCase()+'</h2>'+
                    '</div>'+

                    '<div class="pricing-item-content">'+
                        '<ul class="ht-list list-brand">'+
                        	(loja.ramo_nome?'<li><i class="fas fa-store-alt"></i> Segmento: '+loja.ramo_nome.toLowerCase().split(",")[0]+'</li>':'')+
                        	(loja.loja_site?'<li><i class="fas fa-share-square"></i> <a href="//'+loja.loja_site+'" target="_blank" style="color:#666;">Site</a></li>':'')+
							(loja.loja_telefone?'<li><i class="fas fa-phone"></i> Telefone: '+loja.loja_telefone+'</li>':'')+
                        	(loja.loja_localizacao?'<li><i class="fas fa-map-marker-alt"></i> Localização: '+loja.loja_localizacao+'</li>':'')+
                        '</ul>'+
                    '</div>'+

					(loja.delivery?
						'<div class="pricing-item-footer">'+
							'<a href="'+loja.delivery+'" target="_blank" class="btn btn-bordered"><i class="fas fa-motorcycle"></i> Delivery</a>'+
						'</div>':''
					)+

					(loja.whatsapp?
						'<div class="pricing-item-footer">'+
							'<a href="https://api.whatsapp.com/send?phone=55'+loja.whatsapp+'" target="_blank" class="btn btn-bordered"><i class="fab fa-whatsapp"></i> WhatsApp</a>'+
						'</div>':''
					)+
                '</div>'+
            '</div>'
		); 		
	}	

	//Eventos da busca por nome
	$('[name=filtro_nome]').keyup(function(){
		if($(this).val() != ''){
			$("[name=filtro_letra]").prop("selectedIndex", 0).val(); 
			$("[name=filtro_ramo]").prop("selectedIndex", 0).val(); 			
			_gaq.push(["_trackPageview",""+link+".php?filtro_nome="+$(this).val()]);
			$(".alfabeto li").removeClass("c11");
			filtroNome = $(this).val();
			filtroNome = filtroNome.replace(/&/g, "%26");
			$.ajax({url:"https://sal.madnezz.com.br/api/site/json/loja.asp?tipo="+tipo+"&shopping_id="+shopping_id+"&filtro_nome="+filtroNome+"&full=true&jsoncallback=?",dataType:"json"}).done(function(data){
				$('.loja_lista').html('');
				$.each(data,function(i,loja){					
					loja_lista(loja);
				});  
				if(data.length){
					$('.loja_qtd').hide();
				}else{
					$(".loja_qtd").show().html("Nenhum resultado encontrado!");
				}
			});
		}
	});
	
	//Eventos do seletor de ramos
	$("[name=filtro_ramo]").change(function(){
		if($(this).val() != ''){
			$("[name=filtro_letra]").prop("selectedIndex", 0).val(); 
			$("[name=filtro_nome]").val('');			
			_gaq.push(["_trackPageview","loja.php?filtro_ramo="+$(this).val()]);
			$(this).addClass('active');
			$(".alfabeto").removeClass("active"); 
			$.ajax({url:"https://sal.madnezz.com.br/api/site/json/loja.asp?tipo="+tipo+"&shopping_id="+shopping_id+"&llj=true&full=true&filtro_ramo="+$(this).val()+"&jsoncallback=?",dataType:"json"}).done(function(data){
				$('.loja_lista').html('');
				$.each(data,function(i,loja){					
					loja_lista(loja);
				}); 
				if(data.length){
					$('.loja_qtd').hide();
				}
			});
			$(".loja_lista").show();
			$('.loja_info').hide();
		} 
	});	
	
	//Eventos do alfabeto
	$("[name=filtro_letra]").change(function(){
		if($(this).val() != ''){
			$("[name=filtro_ramo]").prop("selectedIndex", 0).val(); 
			$("[name=filtro_nome]").val('');			
			_gaq.push(["_trackPageview","loja.php?filtro_ramo="+$(this).val()]);
			$.ajax({url:"https://sal.madnezz.com.br/api/site/json/loja.asp?tipo="+tipo+"&shopping_id="+shopping_id+"&filtro_letra="+$(this).val()+"&full=true&jsoncallback=?",dataType:"json"}).done(function(data){
				$('.loja_lista').html('');
					$.each(data,function(i,loja){						
						loja_lista(loja);
					});  
					if(data.length){
						$(".loja_qtd").hide();
					}else{
						$('.loja_lista').html('');
						$(".loja_qtd").show().html("Nenhum resultado encontrado para a letra <span class='green'>"+$(this).val()+"</span>");
					}
			});
		}
	});	
		
	//Função que monta as informações da loja
	//Monta ramos
	function init(){
		$.ajax({url:"https://sal.madnezz.com.br/api/site/json/loja.asp?tipo="+tipo+"&shopping_id="+shopping_id+"&llj=true&full=true&jsoncallback=?",dataType:"json"}).done(function(data){
			$.each(data.ramo,function(i,ramo){
				$("[name=filtro_ramo]").append("<option value='"+ramo.ramo_id+"'>"+ramo.ramo_nome+"</option>");
			});

			function lojaInicial(){
				$.each(data.loja,function(i,loja){  
					loja_lista(loja);
				}); 		
			}
			
			if (!loja_id) {
				lojaInicial();
				if(data.loja.length){
					$(".loja_qtd").hide();
				}else{
					$(".loja_qtd").show().append("<div>Nenhuma loja encontrada!</div>");
				}
			} else{
				monta_loja(loja_id);
			}

			$('.loading').fadeOut(100);
			$(".loja_lista").fadeIn(500);	
			
		});
	}
  	init();
});